package com.telstra.weather.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.telstra.weather.R;
import com.telstra.weather.model.DayWeather;
import com.telstra.weather.utility.DateUtility;

import java.util.Arrays;
import java.util.List;

/**
 * Created by najmussadat on 12/11/16.
 */

public class DayWeatherListAdapter extends RecyclerView.Adapter<DayWeatherListAdapter.DateViewHolder> {

    private List<DayWeather> dayWeathers;
    private RecyclerViewOnItemListener listener;
    private Context mContext;
    private boolean[] selectionStatuses;

    public DayWeatherListAdapter(List<DayWeather> dayWeathers, Context context) {
        this.dayWeathers = dayWeathers;
        this.mContext = context;
    }

    public void setListener(RecyclerViewOnItemListener listener) {
        this.listener = listener;
    }

    public void updateSelectionStatus(int size) {
        selectionStatuses = new boolean[size];
    }

    @Override
    public DateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_date_list, parent, false);

        return new DateViewHolder(itemView);
    }

    public void updateSelection(int position) {
        Arrays.fill(selectionStatuses, false);
        selectionStatuses[position] = true;
    }

    @Override
    public void onBindViewHolder(DateViewHolder holder, int position) {
        holder.bind(position, listener);
    }

    @Override
    public int getItemCount() {
        return dayWeathers.size();
    }

    public interface RecyclerViewOnItemListener {
        void onItemClick(int position);
    }

    public class DateViewHolder extends RecyclerView.ViewHolder {
        TextView date, time, maxTemp, minTemp;
        ImageView weatherIcon;
        LinearLayout mainLayout;

        public DateViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            maxTemp = (TextView) view.findViewById(R.id.maxTemp);
            minTemp = (TextView) view.findViewById(R.id.minTemp);
            weatherIcon = (ImageView) view.findViewById(R.id.weatherIcon);
            mainLayout = (LinearLayout) view.findViewById(R.id.mainLayout);
        }

        void bind(final int position, final RecyclerViewOnItemListener listener) {
            //set the background
            if (selectionStatuses != null && selectionStatuses[position]) {
                mainLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_day_background));
            } else {
                mainLayout.setBackgroundResource(R.drawable.unselected_background);
            }
            DayWeather dayWeather = dayWeathers.get(position);
            //setting the date, if its current date then print TODAY
            date.setText(DateUtils.isToday(DateUtility.getDateFromString(dayWeather.dt_txt, DateUtility.DEFAULT_DATE_FORMAT).getTime()) ? mContext.getString(R.string.today) : DateUtility.getFormattedDayTime(dayWeather.dt_txt, DateUtility.DEFAULT_DATE_FORMAT, "EEE"));
            //setting the time
            time.setText(DateUtility.getFormattedDayTime(dayWeather.dt_txt, DateUtility.DEFAULT_DATE_FORMAT, "hh:mm a"));
            //downloading the weatherIcon
            Picasso.with(mContext).load(String.format(mContext.getString(R.string.weather_icon_url), dayWeather.weather.get(0).icon)).into(weatherIcon);

            //max an min tempearture
            maxTemp.setText(String.format(mContext.getString(R.string.max_temperature),dayWeather.main.temp_max));
            minTemp.setText(String.format(mContext.getString(R.string.max_temperature),dayWeather.main.temp_min));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        updateSelection(position);
                        notifyDataSetChanged();
                        listener.onItemClick(position);
                    }
                }
            });
        }
    }
}
