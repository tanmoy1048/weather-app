package com.telstra.weather.rest;

import com.telstra.weather.model.WeatherResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by najmussadat on 12/11/16.
 */

public interface ApiService {
    @GET("/data/2.5/forecast")
    Call<WeatherResponse> getWeatherByCityId(@Query("id") String id, @Query("APPID") String appId);

    @GET("/data/2.5/forecast")
    Call<WeatherResponse> getWeatherByCoordinate(@Query("lat") Double lat, @Query("lon") Double lon, @Query("units") String units, @Query("APPID") String appId);
}
