package com.telstra.weather.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by najmussadat on 12/11/16.
 */

public class WeatherResponse {
    @SerializedName("city")
    public City city;
    @SerializedName("list")
    public List<DayWeather> list;

    public class City {
        @SerializedName("id")
        public Integer id;
        @SerializedName("name")
        public String name;
        @SerializedName("coord")
        public Coordinate coord;
        @SerializedName("country")
        public String country;
    }

    public class Coordinate {
        @SerializedName("lon")
        public Double lon;
        @SerializedName("lat")
        public Double lat;
    }
}
