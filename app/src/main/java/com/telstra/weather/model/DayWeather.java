package com.telstra.weather.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by najmussadat on 12/11/16.
 */

public class DayWeather {
    @SerializedName("dt_txt")
    public String dt_txt;
    @SerializedName("dt")
    public Long dt;
    @SerializedName("wind")
    public Wind wind;
    @SerializedName("weather")
    public List<Weather> weather;
    @SerializedName("main")
    public WeatherInfo main;

    public class Wind {
        @SerializedName("speed")
        public Double speed;
        @SerializedName("deg")
        public Double deg;
    }

    public class Weather {
        @SerializedName("main")
        public String main;
        @SerializedName("description")
        public String description;
        @SerializedName("icon")
        public String icon;
    }

    public class WeatherInfo {
        @SerializedName("temp")
        public Double temp;
        @SerializedName("temp_min")
        public Double temp_min;
        @SerializedName("temp_max")
        public Double temp_max;
        @SerializedName("pressure")
        public Double pressure;
        @SerializedName("sea_level")
        public Double sea_level;
        @SerializedName("grnd_level")
        public Double grnd_level;
        @SerializedName("humidity")
        public Double humidity;
    }
}
