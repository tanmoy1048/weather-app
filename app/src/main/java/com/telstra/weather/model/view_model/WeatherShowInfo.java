package com.telstra.weather.model.view_model;

/**
 * Created by najmussadat on 12/11/16.
 */

public class WeatherShowInfo {
    public String date;
    public double minTemperature;
    public double maxTemperature;
    public String condition;
    public double wind;
    public double humidity;

    public int responseCount;
}
