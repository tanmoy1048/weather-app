package com.telstra.weather.fragment;

import android.support.v4.app.Fragment;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.telstra.weather.R;
import com.telstra.weather.model.DayWeather;
import com.telstra.weather.utility.DateUtility;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by najmussadat on 12/11/16.
 */
@EFragment(R.layout.fragment_individual_weather)
public class IndividualDayWeatherInfoFragment extends Fragment {
    @ViewById
    TextView location, day, time, humidity, condition, currentTemp, maxTemp, minTemp;
    @ViewById
    ImageView weatherIcon;
    private LocationInformationListener locationInformationListener;

    public void populateData(DayWeather dayWeather) {
        //weather condition
        condition.setText(dayWeather.weather.get(0).description.substring(0,1).toUpperCase() + dayWeather.weather.get(0).description.substring(1));
        //loading weather icon
        Picasso.with(getActivity()).load(String.format(getString(R.string.weather_icon_url), dayWeather.weather.get(0).icon)).into(weatherIcon);
        //temperature
        currentTemp.setText(String.format(getString(R.string.current_temperature),dayWeather.main.temp));
        maxTemp.setText(String.format(getString(R.string.max_temperature),dayWeather.main.temp_max));
        minTemp.setText(String.format(getString(R.string.min_temperature),dayWeather.main.temp_min));
        //humidity
        humidity.setText(String.format(getString(R.string.humidity),dayWeather.main.humidity));
        //day and time
        day.setText(DateUtility.getFormattedDayTime(dayWeather.dt_txt, DateUtility.DEFAULT_DATE_FORMAT, "EEEE"));
        time.setText(DateUtility.getFormattedDayTime(dayWeather.dt_txt, DateUtility.DEFAULT_DATE_FORMAT, "hh:mm a"));
        //location
        if (locationInformationListener != null) {
            location.setText(locationInformationListener.getLocationName());
        }
    }

    public void setLocationInformationListener(LocationInformationListener locationInformationListener) {
        this.locationInformationListener = locationInformationListener;
    }

    public interface LocationInformationListener {
        String getLocationName();
    }
}
