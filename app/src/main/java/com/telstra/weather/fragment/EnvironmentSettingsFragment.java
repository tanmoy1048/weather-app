package com.telstra.weather.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.telstra.weather.R;

/**
 * Created by admin on 22/10/2015.
 */
public class EnvironmentSettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.environement_preferences);
    }
}
