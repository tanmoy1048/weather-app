package com.telstra.weather.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

/**
 * Created by saikat on 3/02/16.
 */
public class MarshMallowPermission {
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    Activity activity;
    Fragment fragment;
    Context context;

    public MarshMallowPermission(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    public MarshMallowPermission(Fragment fragment) {
        this.fragment = fragment;
        this.context = fragment.getContext();
    }

    public boolean isPermissionGrantedForLocation() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermissionForLocation() {
        if (this.fragment != null) {
            fragment.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(activity, "Location permission needed. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
    }
}
