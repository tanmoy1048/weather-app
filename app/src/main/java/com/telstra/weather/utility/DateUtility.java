package com.telstra.weather.utility;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by najmussadat on 12/11/16.
 */

public class DateUtility {
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    final static String TAG = "DateUtility";

    //return current time with given format
    public static String getCurrentTime(String format) {
        return getSimpleFormatter(format).format(Calendar.getInstance().getTime());
    }

    //creating the dateformat
    public static SimpleDateFormat getSimpleFormatter(String format) {
        return new SimpleDateFormat(format, Locale.ENGLISH);
    }

    //changing the date format from given to expected format
    public static String getFormattedDayTime(String dateString, String senderFormat, String expectedFormat) {
        SimpleDateFormat sdf = getSimpleFormatter(senderFormat);
        try {
            return getSimpleFormatter(expectedFormat).format(sdf.parse(dateString));
        } catch (ParseException p) {
            Log.e(TAG, p.getMessage());
        }
        return null;
    }

    //get date instance
    public static Date getDateFromString(String date, String format) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
            return formatter.parse(date);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            return null;
        }
    }


}
