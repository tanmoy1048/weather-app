package com.telstra.weather.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.telstra.weather.R;

/**
 * Created by najmussadat on 12/11/16.
 */
public class SharedPreferenceUtility {
    private static SharedPreferenceUtility ourInstance = new SharedPreferenceUtility();

    private SharedPreferenceUtility() {
    }

    public static SharedPreferenceUtility getInstance() {
        return ourInstance;
    }

    //get the base URL for weather API call
    public String getBaseUrl(Context context) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        return SP.getString(context.getString(R.string.base_url), "http://api.openweathermap.org/"); //by default production URL
    }

    //get the base URL for weather API call
    public String getUnitPreference(Context context) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        return SP.getString(context.getString(R.string.setting_preference), "metric"); //by default unit is metric for celcius
    }

    //write the base URL for weather API call
    public void writeBaseUrl(Context context, String url) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = SP.edit();
        editor.putString(context.getString(R.string.base_url), url).apply();
    }

    //get last location
    public LatLng getLastLocation(Context context) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        String json= SP.getString(context.getString(R.string.last_location), null);
        if(json==null)
            return null;

        return new Gson().fromJson(json, LatLng.class);
    }

    public void writeLocation(Context context, LatLng latLng) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = SP.edit();
        Gson gson = new Gson();
        String json = gson.toJson(latLng);
        prefsEditor.putString(context.getString(R.string.last_location), json);
        prefsEditor.apply();
    }
}
