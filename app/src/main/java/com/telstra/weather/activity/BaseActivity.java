package com.telstra.weather.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;

@EActivity
public class BaseActivity extends AppCompatActivity {

    public ProgressDialog mProgressDialog;

    //show a progress dialog
    @UiThread
    public void showProgressDialog(int res) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(res));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    //hide the dialog
    @UiThread
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        hideProgressDialog();
    }

}
