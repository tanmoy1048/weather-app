package com.telstra.weather.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.telstra.weather.R;
import com.telstra.weather.adapter.DayWeatherListAdapter;
import com.telstra.weather.fragment.IndividualDayWeatherInfoFragment;
import com.telstra.weather.fragment.IndividualDayWeatherInfoFragment_;
import com.telstra.weather.model.DayWeather;
import com.telstra.weather.model.WeatherResponse;
import com.telstra.weather.rest.RestClient;
import com.telstra.weather.utility.MarshMallowPermission;
import com.telstra.weather.utility.SharedPreferenceUtility;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static com.telstra.weather.activity.SettingsActivity.PREFERENCE.ENVIRONMENT_PREFERENCE;
import static com.telstra.weather.activity.SettingsActivity.PREFERENCE.UNIT_PREFERENCE;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, IndividualDayWeatherInfoFragment.LocationInformationListener {
    private final String TAG = "MainActivity";

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private final int SETTING_REQUEST_CODE = 2;
    private final int GOOGLE_API_CLIENT_ID = 0;
    protected GoogleApiClient mGoogleApiClient;
    private LatLng latLng;

    @ViewById
    RecyclerView dateList;
    @ViewById
    CoordinatorLayout coordinatorLayout;

    //Fragment to populate detail data
    IndividualDayWeatherInfoFragment individualDayWeatherInfoFragment;

    private String baseUrl;
    private String APP_KEY;
    private String unit;
    private DayWeatherListAdapter dayListAdapter;
    private String locationName = "";
    //weather response list
    private List<DayWeather> dayWeathers = new ArrayList<>();

    @AfterViews
    void init() {
        baseUrl = SharedPreferenceUtility.getInstance().getBaseUrl(this);
        APP_KEY = getString(R.string.weather_api_key);
        unit = SharedPreferenceUtility.getInstance().getUnitPreference(this);

        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        LinearLayoutManager layoutManager;
        if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
            //making the horizontal list for the dates
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        } else {
            //making the vertical list for the dates
            layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        }
        dateList.setLayoutManager(layoutManager);

        //create the adapter and set to the list
        dayListAdapter = new DayWeatherListAdapter(dayWeathers, this);
        dayListAdapter.setListener(new DayWeatherListAdapter.RecyclerViewOnItemListener() {
            @Override
            public void onItemClick(int position) {
                individualDayWeatherInfoFragment.populateData(dayWeathers.get(position));
            }
        });
        dateList.setAdapter(dayListAdapter);

        //adding the fragment to the view
        individualDayWeatherInfoFragment = IndividualDayWeatherInfoFragment_.builder().build();
        individualDayWeatherInfoFragment.setLocationInformationListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentHolder, individualDayWeatherInfoFragment).commit();

        //trying to retrieve the saved place from shsared preference
        latLng = SharedPreferenceUtility.getInstance().getLastLocation(this);
        if (latLng == null) {
            showLocationError();
        } else {
            //get the weather
            getWeather(baseUrl, latLng.latitude, latLng.longitude, unit, APP_KEY);
        }

        if (getResources().getBoolean(R.bool.setting_screen)) {
            SettingsActivity_.intent(this).preference(ENVIRONMENT_PREFERENCE).startForResult(SETTING_REQUEST_CODE);
        }
    }

    //if location is not found
    private void showLocationError() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.location_message))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).show();
    }

    //get the weather using a coordinate
    @Background
    void getWeather(String baseUrl, Double lat, Double lng, String unit, String key) {
        showProgressDialog(R.string.getting_weather);
        Call<WeatherResponse> call = new RestClient(baseUrl).getApiService().getWeatherByCoordinate(lat, lng, unit, key);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            @UiThread
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    dayWeathers.clear();
                    WeatherResponse weatherResponse = response.body();
                    locationName = weatherResponse.city.name;
                    dayWeathers.addAll(weatherResponse.list);
                    dayListAdapter.updateSelectionStatus(dayWeathers.size());
                    dayListAdapter.updateSelection(0);
                    dayListAdapter.notifyDataSetChanged();
                    if (dayWeathers.size() > 0) {
                        individualDayWeatherInfoFragment.populateData(dayWeathers.get(0));
                    }
                    Log.d(TAG, weatherResponse.city.name);
                } else {
                    Snackbar.make(coordinatorLayout, getString(R.string.network_error), Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                hideProgressDialog();
                Snackbar.make(coordinatorLayout, t.getMessage(), Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                //opening the google place widget
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                }
                return true;
            case R.id.action_gps:
                //check the marshmellow permission
                MarshMallowPermission marshMallowPermission = new MarshMallowPermission(this);
                if (!marshMallowPermission.isPermissionGrantedForLocation()) {
                    marshMallowPermission.requestPermissionForLocation();
                } else {
                    //if the permission is already been granted earlier
                    getCurrentLocation();
                }
                break;
            case R.id.action_setting:
                //unit preference
                SettingsActivity_.intent(this).preference(UNIT_PREFERENCE).startForResult(SETTING_REQUEST_CODE);
                break;
        }
        return false;
    }

    /**
     * getting the current location and its coordinates
     */
    void getCurrentLocation() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        showProgressDialog(R.string.getting_location);
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(PlaceLikelihoodBuffer likelyPlaces) {
                hideProgressDialog();
                if (!likelyPlaces.getStatus().isSuccess()) {
                    // Request did not complete successfully
                    Snackbar.make(coordinatorLayout, getString(R.string.location_error), Snackbar.LENGTH_LONG).show();
                    likelyPlaces.release();
                    return;
                }
                //if successful, get the place detail
                latLng = likelyPlaces.get(0).getPlace().getLatLng();
                getWeather(baseUrl, latLng.latitude, latLng.longitude, unit, APP_KEY);
                SharedPreferenceUtility.getInstance().writeLocation(MainActivity.this, likelyPlaces.get(0).getPlace().getLatLng());
                likelyPlaces.release();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PLACE_AUTOCOMPLETE_REQUEST_CODE:
                    //retrieve the place from google place widget
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    latLng = place.getLatLng();
                    SharedPreferenceUtility.getInstance().writeLocation(MainActivity.this, place.getLatLng());
                    getWeather(baseUrl, place.getLatLng().latitude, place.getLatLng().longitude, unit, APP_KEY);
                    Log.i(TAG, "Place: " + place.getName());
                    break;
            }
        }
        if (requestCode == SETTING_REQUEST_CODE) {
            //if unit preference and url are changed then get the weather
            unit = SharedPreferenceUtility.getInstance().getUnitPreference(this);
            baseUrl = SharedPreferenceUtility.getInstance().getBaseUrl(this);
            if (latLng != null) {
                getWeather(baseUrl, latLng.latitude, latLng.longitude, unit, APP_KEY);
            } else {
                showLocationError();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MarshMallowPermission.LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getCurrentLocation();
                        }
                    }, 100);
                } else {
                    Snackbar.make(coordinatorLayout, getString(R.string.location_permission_denied), Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Snackbar.make(coordinatorLayout, connectionResult.getErrorMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public String getLocationName() {
        return locationName;
    }
}
