package com.telstra.weather.activity;

import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.telstra.weather.R;
import com.telstra.weather.fragment.EnvironmentSettingsFragment;
import com.telstra.weather.fragment.PreferenceSettingsFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;

/**
 * Created by admin on 21/10/2015.
 */
@EActivity(R.layout.activity_preference)
public class SettingsActivity extends AppCompatActivity {
    @Extra
    PREFERENCE preference;

    @AfterViews
    void setupUI() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (preference != null) {
            switch (preference) {
                case ENVIRONMENT_PREFERENCE:
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new EnvironmentSettingsFragment()).commit();
                    break;
                case UNIT_PREFERENCE:
                    getFragmentManager().beginTransaction().replace(R.id.content_frame, new PreferenceSettingsFragment()).commit();
                    break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return false;
    }

    public enum PREFERENCE {
        ENVIRONMENT_PREFERENCE,
        UNIT_PREFERENCE
    }
}
