﻿Environment Selection:
Whenever the app starts, a new activity will pop up and ask for the environment selection. By default, it will point to the production URL.


This activity will pop up only the build is debug version. If we make a release build of this project, this will always point to the production and that environment setting activity will not start at the beginning. This has been done as boolean_values.xml has been put in debug and main folder of the project as true and false respectively for starting the setting activity.


This is app is using OpenWeatherMap (http://openweathermap.org/) and fetching five days of weather forcasts. At this moment, there is not mock api server has been set. However any of the mock/stub server can be used as the development purpose.


Location Setting:
For the first time, the app will show an alert for selecting the location for the weather api call.


I am using Google Place library to get the locations. The GPS menu at the top will try to get the current location and the search menu will pop up a autocomplete text widget to take the user input for the location.


Since I am using the Google Place, there is a need of API_KEY and the sha1 needs to be added in Google Developer console. The API_KEY from the Google console needs to be put in ids.xml. 


Unit Setting:
The user will be able to change the unit of the temperature from the setting menu from the app.


Orientation Support:
The app has both portrait and horizontal view support.